-- информация об остатке часов по дисциплинам
-- по видам занятий (общее количество часов
-- минус количество часов занятий в расписании)

SELECT
disc.nd,
vid_zanyatii.vz,
Sum(chasi_disc.chasi) AS vsego_chasov,
Sum(raspisanie.count_hours) AS provedeno_chasov,
(Sum(chasi_disc.chasi)-Sum(raspisanie.count_hours)) AS ostalos_chasov
FROM
disc
INNER JOIN disc_spec ON disc_spec.id_disc = disc.id_disc
INNER JOIN chasi_disc ON chasi_disc.id_ds = disc_spec.id_ds
INNER JOIN vid_zanyatii ON chasi_disc.id_vz = vid_zanyatii.id_vz
INNER JOIN teacher_gr ON teacher_gr.id_cd = chasi_disc.id_cd
INNER JOIN raspisanie ON raspisanie.id_tg = teacher_gr.id_tg
GROUP BY
disc.nd,
vid_zanyatii.vz


-- среднее количество часов занятий в день по
-- аудиториям

SELECT
Avg(raspisanie.count_hours) AS avg_hours,
lecture_rooms.num_lr,
raspisanie.date_zan
FROM
lecture_rooms
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
GROUP BY
lecture_rooms.num_lr,
raspisanie.date_zan


-- средняя загрузка студентов в день


SELECT
raspisanie.date_zan,
Sum(raspisanie.count_hours) AS total_hours
FROM
raspisanie
GROUP BY
raspisanie.date_zan



-- общие итоги о количестве проведенных часов
-- занятий
--  -по группам
--  -по преподавателям
--  -по аудиториям


SELECT
gr.ng,
teachers.surname,
teachers.`name`,
teachers.middlename,
SUM(raspisanie.count_hours) AS total_hours,
lecture_rooms.num_lr
FROM
lecture_rooms
INNER JOIN raspisanie ON raspisanie.id_lr = lecture_rooms.id_lr
INNER JOIN teacher_gr ON raspisanie.id_tg = teacher_gr.id_tg
INNER JOIN gr ON teacher_gr.id_gr = gr.id_gr
INNER JOIN teachers ON teacher_gr.id_t = teachers.id_t
GROUP BY
gr.ng,
lecture_rooms.num_lr,
teachers.surname,
teachers.`name`,
teachers.middlename


